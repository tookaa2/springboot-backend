package com.example.demo.config

import com.example.demo.entity.*
import com.example.demo.repository.*
import com.example.demo.security.entity.Authority
import com.example.demo.security.entity.AuthorityName
import com.example.demo.security.entity.JwtUser
import com.example.demo.security.repository.AuthorityRepository
import com.example.demo.security.repository.UserRepository
import com.example.demo.utils.SeatPlanFactory
import com.example.demo.utils.SeatPlanFactoryInterface
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Component
import java.sql.Timestamp
import java.time.LocalDate
import java.util.*
import javax.transaction.Transactional

@Component
class ApplicationLoader: ApplicationRunner{
    @Autowired
    lateinit var movieRepository: MovieRepository
    @Autowired
    lateinit var seatRepository: SeatRepository
    @Autowired
    lateinit var cinemaRepository: CinemaRepository
    @Autowired
    lateinit var seatObjectRepository: SeatObjectRepository
    @Autowired
    lateinit var showtimeRepository: ShowtimeRepository
    @Autowired
    lateinit var seatPlanFactoryInterface: SeatPlanFactoryInterface
    @Autowired
    lateinit var customerRepository: CustomerRepository
    @Autowired
    lateinit var dataLoader: DataLoader
    @Autowired
    lateinit var authorityRepository: AuthorityRepository
    @Autowired
    lateinit var userRepository: UserRepository
    @Transactional
    fun loadUsernameAndPassword(){
        val auth1 = Authority(name= AuthorityName.ROLE_ADMIN)
        val auth2= Authority(name= AuthorityName.ROLE_CUSTOMER)
        val auth3= Authority(name= AuthorityName.ROLE_GENERAL)
        authorityRepository.save(auth1)
        authorityRepository.save(auth2)
        authorityRepository.save(auth3)
        val encoder = BCryptPasswordEncoder()
//        val cus1 = Customer(name="สมชาติ",email="a@b.com")
        var customer1 = customerRepository.save( Customer("Admin","Admin","admin@email.com","admin",UserStatus.ACTIVE))
        val custJwt = JwtUser(
                username = "admin",
                password = encoder.encode("admin"),
                email = customer1.email,
                enabled = true,
                firstname = customer1.firstname,
                lastname = customer1.lastname
        )
        customerRepository.save(customer1)
        userRepository.save(custJwt)
        customer1.jwtUser=custJwt
        custJwt.user=customer1
        custJwt.authorities.add(auth1)
        custJwt.authorities.add(auth2)
        custJwt.authorities.add(auth3)
    }

    @Transactional
    override fun run(args: ApplicationArguments?){

//        var movie1=movieRepository.save(Movie("The incredible", 120,"somestring"))
//        movie1.subtitle=arrayOf("en","th")
//        movie1.soundtrack=arrayOf("en","th")

        var seatSets1=seatRepository.save(Seat(SeatType.DELUXE,150,10,22))
        var seatSets2=seatRepository.save(Seat(SeatType.PREMIUM,190,3,22))
        var seatSets3=seatRepository.save(Seat(SeatType.SOFA,500,1,12))
        var cinema1 = cinemaRepository.save(Cinema("Cinema1"))
        cinema1.seat= mutableListOf(seatSets1,seatSets2,seatSets3)

        var seatSets1A=seatRepository.save(Seat(SeatType.DELUXE,150,8,18))
        var seatSets2A=seatRepository.save(Seat(SeatType.PREMIUM,190,1,18))
        var seatSets3A=seatRepository.save(Seat(SeatType.SOFA,500,1,6))
        var cinema3 = cinemaRepository.save(Cinema("Cinema3"))
        cinema3.seat= mutableListOf(seatSets1A,seatSets2A,seatSets3A)

        var seatSets1B=seatRepository.save(Seat(SeatType.DELUXE,150,10,20))
        var seatSets2B=seatRepository.save(Seat(SeatType.PREMIUM,190,4,20))
        var cinema2 = cinemaRepository.save(Cinema("Cinema2"))
        cinema2.seat= mutableListOf(seatSets1B,seatSets2B)

//        var showtime1=showtimeRepository.save(Showtime(
//                Timestamp(1553412600).time,
//                Timestamp(1553419800000).time,
//                movie1
//        ))
//
//        showtime1.seats.addAll(seatPlanFactoryInterface.parseCinemaSeat(cinema1.seat))

        dataLoader.loadData()
        loadUsernameAndPassword()
    }
}