package com.example.demo.config


import com.example.demo.entity.Movie
import com.example.demo.entity.Showtime
import com.example.demo.repository.*
import com.example.demo.utils.SeatPlanFactoryInterface
import org.apache.commons.math3.stat.descriptive.summary.Product
import org.apache.poi.ss.usermodel.CellType
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.util.CellReference
import org.apache.poi.ss.util.CellUtil
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.ClassPathResource
import org.springframework.stereotype.Component
import java.io.IOException
import java.sql.Timestamp
import javax.transaction.Transactional


@Component
class DataLoader {
    @Value("\${xlsxPath}")
    val xlsxPath: String? = null
    //    @Autowired
//    lateinit var manufacturerRepository: ManufacturerRepository
//    @Autowired
//    lateinit var productRepository: ProductRepository
    @Autowired
    lateinit var showtimeRepository: ShowtimeRepository
    @Autowired
    lateinit var cinemaRepository: CinemaRepository
    @Autowired
    lateinit var seatPlanFactoryInterface: SeatPlanFactoryInterface
    @Autowired
    lateinit var movieRepository: MovieRepository

    fun loadData() {
        createMovieData()
        createShowtimeData()
    }

    fun getCellData(row: Row, col: String): String {
        val colIdx = CellReference.convertColStringToIndex(col)
        return getCellData(row, colIdx)
    }

    fun getCellData(row: Row, colIdx: Int): String {
        val cell = CellUtil.getCell(row, colIdx)
        if (cell.cellType == CellType.STRING) {
            if (cell.stringCellValue == "NULL")
                return ""
            return cell.stringCellValue
        } else if (cell.cellType == CellType.NUMERIC) {
            cell.cellType = CellType.STRING
            return cell.stringCellValue
        }
        return ""
    }

    @Transactional
    fun createShowtimeData() {
        try {
            val file = xlsxPath?.let { ClassPathResource(it).inputStream }
            val workbook = XSSFWorkbook(file)
            val sheet = workbook.getSheet("showtime")
            val rowIterator = sheet.iterator()
            rowIterator.next()
            while (rowIterator.hasNext()) {
                val row = rowIterator.next()
                if (getCellData(row, "A") != "") {
                    var movie = movieRepository.findByName(getCellData(row, "A"))
                    movie?.let {

                        var showtime = showtimeRepository.save(Showtime(
                                Timestamp(getCellData(row, "B").toLong()).time,
                                Timestamp(getCellData(row, "C").toLong()).time,
                                movie,
                                cinemaRepository.findByName(getCellData(row, "D"))!!

                        ))

                        showtime.seats.addAll(seatPlanFactoryInterface.parseCinemaSeat(
                                cinemaRepository.findByName(getCellData(row, "D"))!!.seat

                        ))
                        showtime.soundtrack = getCellData(row, "E")
                        showtime.subtitle = getCellData(row, "F")
                    }


                }


            }
        } catch (e: IOException) {

        }
    }

    @Transactional
    fun createMovieData() {
        try {
            val file = xlsxPath?.let { ClassPathResource(it).inputStream }
            val workbook = XSSFWorkbook(file)
            val sheet = workbook.getSheet("movies")
            val rowIterator = sheet.iterator()
            rowIterator.next()
            while (rowIterator.hasNext()) {
                val row = rowIterator.next()
                if (getCellData(row, "A") != "") {


                    var movie = movieRepository.save(
                            Movie(
                                    getCellData(row, "A"),
                                    getCellData(row, "B").toInt(),
                                    getCellData(row, "C"),
                                    getCellData(row, "F")
                                    ))
                    movie.subtitle = getCellData(row, "D").split(",").toTypedArray()
                    movie.soundtrack = getCellData(row, "E").split(",").toTypedArray()


                }


            }
        } catch (e: IOException) {

        }
    }


}