package com.example.demo.entity


import java.time.LocalDate
import javax.persistence.*

@Entity
data class Showtime(

        var startDateTime: Long,
        var endDateTime:Long,
        @OneToOne
        var movie:Movie?=null,
        @ManyToOne
        var cinema:Cinema,
        var soundtrack:String?="en",
        var subtitle:String?="en"

){
        @Id
        @GeneratedValue
        var id:Long? = null
        @ManyToMany
        var seats = mutableListOf<SeatObject>()
}