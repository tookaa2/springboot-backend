package com.example.demo.entity
enum class SeatType {
    PREMIUM,
    DELUXE,
    SOFA
}