package com.example.demo.entity

import java.util.*
import javax.persistence.*

@Entity
data class ShowtimeSeat(
        var soundtracks:String,
        var subtitles:String,
        var startTime:Date,
        var endTime:Date,
        @OneToOne
        var movie: Movie?=null


){
        @OneToMany
        var seats=mutableListOf<SeatObject>()
        @Id
        @GeneratedValue
        var id:Long? = null
}