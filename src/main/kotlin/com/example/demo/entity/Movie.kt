package com.example.demo.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToMany


@Entity
data class Movie(
        var name: String,
        var duration: Int,
        var image: String,
        var youtubeId:String? = null,
        var soundtrack: Array<String>? = null,
        var subtitle: Array<String>? = null,
        var isDeleted:Boolean = false

) {
    @Id
    @GeneratedValue
    var id: Long? = null

}