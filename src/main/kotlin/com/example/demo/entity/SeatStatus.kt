package com.example.demo.entity

enum class SeatStatus{
    EMPTY,
    BOOKED,
    RESERVED
}