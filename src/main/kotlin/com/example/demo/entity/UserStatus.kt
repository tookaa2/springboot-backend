package com.example.demo.entity

enum class  UserStatus{
    PENDING,ACTIVE,NOTACTIVE,DELETED
}