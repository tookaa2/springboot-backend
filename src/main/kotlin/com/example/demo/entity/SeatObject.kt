package com.example.demo.entity

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class SeatObject(
        var status: SeatStatus? = SeatStatus.EMPTY,
        @Column(name="seatObjectRow")
        var row: String? = null,
        @Column(name="seatObjectColumn")
        var column: String? = null,
        var type: SeatType?= SeatType.DELUXE,
        var price:Int?=0
) {
    @Id
    @GeneratedValue
    var id: Long? = null
}