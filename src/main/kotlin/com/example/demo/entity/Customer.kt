package com.example.demo.entity

import javax.persistence.*

@Entity
data class Customer(

    override var firstname:String?=null,
    override var lastname:String?=null,
    override var email:String?=null,
    override var password:String?=null,
    override var userStatus: UserStatus?= UserStatus.PENDING,
    override var imageUrl:String?=null
) :User(firstname,lastname,email,userStatus){

    var isDeleted:Boolean?=false

    @ManyToMany
    var bookingList = mutableListOf<Booking>()

}