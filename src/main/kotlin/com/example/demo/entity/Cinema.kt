package com.example.demo.entity

import javax.persistence.*

@Entity
data class Cinema(
        var name: String? = null

) {
    @Id
    @GeneratedValue
    var id: Long? = null
    @OneToMany
    var seat = mutableListOf<Seat>()
}