package com.example.demo.entity

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class Seat(
        var type: SeatType? = SeatType.DELUXE,
        var price: Int? = 0,
        @Column(name="seatRow")
        var rows:Int?=0,
        @Column(name="seatColumn")
        var column:Int?=0
        ){
        @Id
        @GeneratedValue
        var id: Long? = null
}