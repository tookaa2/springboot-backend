package com.example.demo.entity

import java.time.LocalDate
import javax.persistence.*

@Entity
class Booking(
        @ManyToOne
        var showtime: Showtime,
        var purchasedTime:Long,
        @ManyToOne
        var customer: Customer,
        var qrCode:String
){
    @Id
    @GeneratedValue
    var id: Long? = null
    @OneToMany
    var seats= mutableListOf<SeatObject>()
}