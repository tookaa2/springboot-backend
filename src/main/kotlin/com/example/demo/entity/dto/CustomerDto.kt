package com.example.demo.entity.dto

import com.example.demo.entity.UserStatus

data class CustomerDto(
        var id: Long?=null,
        var firstname: String? = null,
        var lastname: String? = null,
        var email: String? = null,
        var userStatus: UserStatus? = UserStatus.PENDING,
        var isDeleted: Boolean? = false,
        var bookingList: List<BookingNoCustomer> = emptyList<BookingNoCustomer>(),
        var imageUrl: String? = null
)