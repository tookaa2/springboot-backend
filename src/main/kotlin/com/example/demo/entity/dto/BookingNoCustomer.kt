package com.example.demo.entity.dto

import com.example.demo.entity.SeatObject
import java.time.LocalDate

data class BookingNoCustomer(
        var showtime: ShowtimeDto?=null,
        var purchasedTime: Long?=null,
        var id: Long?=null,
        var seats :List<SeatObject>? = emptyList<SeatObject>(),
        var qrCode:String?=null
)
