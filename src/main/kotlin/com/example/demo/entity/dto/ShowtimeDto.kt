package com.example.demo.entity.dto

import com.example.demo.entity.Cinema
import com.example.demo.entity.Movie
import java.time.LocalDate

data class ShowtimeDto(
        var startDateTime: Long?=null,
        var endDateTime: Long?=null,
        var movie: Movie?=null,
        var soundtrack:String?=null,
        var subtitle:String?=null,
        var id:Long?=null,
        var cinema: Cinema?=null
)