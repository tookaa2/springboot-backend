package com.example.demo.entity.dto

import com.example.demo.entity.SeatObject
import java.time.LocalDate

data class BookingRequest(
        var showtimeId:Long?=null,
        var seats: List<SeatObject>
)