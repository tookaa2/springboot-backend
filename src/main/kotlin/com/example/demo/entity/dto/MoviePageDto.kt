package com.example.demo.entity.dto

import com.example.demo.entity.Movie

data class MoviePageDto(
        var totalPages: Int? = null,
        var totalElements: Long? = null,
        var movies: List<Movie>? = mutableListOf()
)