package com.example.demo.entity.dto

import com.example.demo.entity.Customer
import com.example.demo.entity.SeatObject
import com.example.demo.entity.Showtime
import java.time.LocalDate

data class BookingDto(
        var showtime: ShowtimeDto?=null,
        var purchasedTime: Long?=null,
        var customer: CustomerDto?=null,
        var id: Long?=null,
        var seats :List<SeatObject>? = emptyList<SeatObject>(),
        var qrCode:String?=null
)