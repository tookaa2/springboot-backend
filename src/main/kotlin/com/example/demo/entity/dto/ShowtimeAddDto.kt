package com.example.demo.entity.dto

data class ShowtimeAddDto(
    var movieTitle:String?=null,
    var startTime:String?=null,
    var cinemaName:String?=null,
    var soundtrack:String?=null,
    var subtitle:String?=null
)