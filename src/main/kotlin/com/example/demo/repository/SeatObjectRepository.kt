package com.example.demo.repository

import com.example.demo.entity.SeatObject
import org.springframework.data.repository.CrudRepository

interface SeatObjectRepository: CrudRepository<SeatObject, Long>