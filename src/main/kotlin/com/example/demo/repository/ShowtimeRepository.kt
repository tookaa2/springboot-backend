package com.example.demo.repository

import com.example.demo.entity.Showtime
import org.springframework.data.repository.CrudRepository

interface ShowtimeRepository: CrudRepository<Showtime, Long>{
    fun findByMovieId(id:Long):List<Showtime>?
}