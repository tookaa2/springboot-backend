package com.example.demo.repository

import com.example.demo.entity.Seat
import org.springframework.data.repository.CrudRepository

interface SeatRepository: CrudRepository<Seat, Long>