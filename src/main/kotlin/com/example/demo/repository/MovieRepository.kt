package com.example.demo.repository

import com.example.demo.entity.Movie
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.CrudRepository

interface MovieRepository: CrudRepository<Movie, Long> {
    fun findByName(cellData: String): Movie?
    fun findByIsDeletedIsFalse():List<Movie>?
    fun findAll(page: Pageable): Page<Movie>
}