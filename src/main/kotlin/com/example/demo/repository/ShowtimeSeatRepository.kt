package com.example.demo.repository

import com.example.demo.entity.Showtime
import org.springframework.data.repository.CrudRepository

interface ShowtimeSeatRepository: CrudRepository<Showtime, Long>