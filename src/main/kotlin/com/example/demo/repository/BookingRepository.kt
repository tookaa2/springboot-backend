package com.example.demo.repository

import com.example.demo.entity.Booking
import org.springframework.data.repository.CrudRepository

interface BookingRepository: CrudRepository<Booking, Long>