package com.example.demo.repository

import com.example.demo.entity.Cinema
import org.springframework.data.repository.CrudRepository

interface CinemaRepository : CrudRepository<Cinema, Long> {
    fun findByName(cellData: String): Cinema?
}