package com.example.demo.utils

import com.example.demo.entity.Seat
import com.example.demo.entity.SeatObject
import com.example.demo.entity.Showtime

interface SeatPlanFactoryInterface{
    fun GenerateAndSaveSeats(plan: Seat, startRowIndex:Int): List<SeatObject>
    fun parseCinemaSeat(seatList:List<Seat>): List<SeatObject>
    fun sortSeat(seatList:List<Seat>):List<Seat>
}