package com.example.demo.utils

import com.example.demo.entity.*
import com.example.demo.entity.dto.*
import org.mapstruct.InheritInverseConfiguration
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings
import org.mapstruct.factory.Mappers

@Mapper(componentModel = "spring")
interface MapperUtil{
    companion object {
        val INSTANCE = Mappers.getMapper(MapperUtil::class.java)
    }
    fun mapShowtimeDto(showtime: Showtime?):ShowtimeDto?
    fun mapBookingDto(booking: Booking?):BookingDto
    fun mapCustomerDto(customer: Customer?):CustomerDto
    fun mapCustomerDto(customer: List<Customer>?):List<CustomerDto>
    fun mapBookingNoCustomer(booking: Booking?):BookingNoCustomer
    fun mapBookingNoCustomer(booking: List<Booking>?):List<BookingNoCustomer>
}
