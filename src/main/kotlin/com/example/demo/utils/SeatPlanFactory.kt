package com.example.demo.utils

import com.example.demo.entity.*
import com.example.demo.repository.SeatObjectRepository
import com.example.demo.repository.ShowtimeRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Component
import org.springframework.stereotype.Repository
import org.springframework.stereotype.Service

@Profile("db")
@Repository
class SeatPlanFactory :SeatPlanFactoryInterface{
    @Autowired
    lateinit var seatObjectRepository: SeatObjectRepository
    @Autowired
    lateinit var showtimeRepository: ShowtimeRepository
    override fun GenerateAndSaveSeats(plan: Seat, startRowIndex:Int): List<SeatObject> {
        var output = mutableListOf<SeatObject>()

            var seatLabel: Char = 'A'
            seatLabel+=startRowIndex-1
            for (i in 1..plan.rows!!) {
                for (k in 1..plan.column!!) {
                    if (plan.type !== SeatType.SOFA) {
                        var seatToAdd=seatObjectRepository.save(SeatObject(SeatStatus.EMPTY,"$seatLabel","$k",plan.type!!,plan.price))
                        output.add(seatToAdd)
                    }else{
                        var seatToAdd=seatObjectRepository.save(SeatObject(SeatStatus.EMPTY,"$seatLabel$seatLabel","$k",plan.type!!,plan.price))
                        output.add(seatToAdd)
                    }
                }
                ++seatLabel
            }

        return output
    }
    override fun parseCinemaSeat(seatList:List<Seat>):List<SeatObject>{
        var startingSeatIndex=1
        val orderedSeat=sortSeat(seatList)
        var output=mutableListOf<SeatObject>()
        for(eachSeatType in orderedSeat){
            if(eachSeatType.type===SeatType.SOFA){
                output.addAll(GenerateAndSaveSeats(eachSeatType,startingSeatIndex))
            }
            else if(eachSeatType.type===SeatType.PREMIUM){
                output.addAll(GenerateAndSaveSeats(eachSeatType,startingSeatIndex))
                startingSeatIndex+=eachSeatType.rows!!
            }else{
                output.addAll(GenerateAndSaveSeats(eachSeatType,startingSeatIndex))
            }

        }
        return output

    }
    override fun sortSeat(seatList:List<Seat>):List<Seat>{
        var output = mutableListOf<Seat>()
        for(seat in seatList){
            if(seat.type===SeatType.SOFA){
                output.add(seat)
            }
        }
        for(seat in seatList){
            if(seat.type===SeatType.PREMIUM){
                output.add(seat)
            }
        }
        for(seat in seatList){
            if(seat.type===SeatType.DELUXE){
                output.add(seat)
            }
        }
        return output
    }
}