package com.example.demo.controller

import com.example.demo.entity.Customer
import com.example.demo.entity.dto.ShowtimeAddDto
import com.example.demo.entity.dto.ShowtimeRequest
import com.example.demo.service.ShowtimeService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class ShowtimeController {
    @Autowired
    lateinit var showtimeService: ShowtimeService

    @PostMapping("/showtimes")
    fun getAllMovies(): ResponseEntity<Any> {
        return ResponseEntity.ok(showtimeService.getAllShowtime())
    }
    @PostMapping("/showtime/add")
    fun addShowtime( @RequestBody showtimeAddDto: ShowtimeAddDto): ResponseEntity<Any> {
        return ResponseEntity.ok(showtimeService.saveShowtime(showtimeAddDto))
    }

    @PostMapping("/movies/showtimes/movie/{movieId}")
    fun getShowtimeByMovieId(
            @RequestBody showtimeRequest: ShowtimeRequest,
            @PathVariable("movieId") id: Long)
            : ResponseEntity<Any> {

        var ouput=showtimeService.getShowtimeByMovie(id, showtimeRequest)
        ouput?.let { return ResponseEntity.ok(it) }
        return ResponseEntity(HttpStatus.NOT_FOUND)

    }

}

