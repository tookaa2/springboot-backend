package com.example.demo.controller

import com.example.demo.entity.dto.BookingRequest
import com.example.demo.service.BookingService
import com.example.demo.utils.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class BookingController{
    @Autowired
    lateinit var bookingService: BookingService
    @PostMapping("/bookings")
    fun getAllBooking(): ResponseEntity<Any> {
        return ResponseEntity.ok(bookingService.getAllBooking())
    }
    @PostMapping("/booking/{customerId}")
    fun addBooking(
            @PathVariable("customerId") id:Long?, @RequestBody bookingRequest: BookingRequest
    ):ResponseEntity<Any>{

        if(id==null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("id must not be null")

        var output=bookingService.save(id,bookingRequest)
            return ResponseEntity.ok(MapperUtil.INSTANCE.mapBookingDto(output)
                    )

    }
}