package com.example.demo.controller

import com.example.demo.service.SeatObjectService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class SeatObjectController{
    @Autowired
    lateinit var seatObjectService: SeatObjectService

    @PostMapping("/seatObjects")
    fun getAllMovies(): ResponseEntity<Any> {
        return ResponseEntity.ok(seatObjectService.getSeats())
    }
}