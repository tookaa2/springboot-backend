package com.example.demo.controller

import com.example.demo.entity.Movie
import com.example.demo.entity.dto.MoviePageDto
import com.example.demo.service.MovieService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.xml.ws.Response

@RestController
class MovieController {
    @Autowired
    lateinit var movieService: MovieService

    @PostMapping("/movies")
    fun getAllMovies(): ResponseEntity<Any> {
        return ResponseEntity.ok(movieService.getMovies())
    }

    @DeleteMapping("/movie/{movieId}")
    fun removeMovie(@PathVariable("movieId") id: Long?): ResponseEntity<Any> {

        return ResponseEntity.ok(movieService.deleteMovie(id))
    }

    @GetMapping("/movie/page")
    fun getMovieWithPage(
            @RequestParam("page") page:Int,
            @RequestParam("pageSize") pageSize:Int
    ):ResponseEntity<Any>{
        val output = movieService.getMoviesWithPage(page,pageSize);
        val movieList = mutableListOf<Movie>()
        for (data in output.content){
            movieList.add(data)
        }
        return ResponseEntity.ok(MoviePageDto(totalPages = output.totalPages,
                totalElements = output.totalElements,
                movies = movieList)
        )
    }

}