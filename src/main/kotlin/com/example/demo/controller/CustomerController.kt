package com.example.demo.controller

import com.example.demo.entity.Customer
import com.example.demo.repository.CustomerRepository
import com.example.demo.service.AmazonClient
import com.example.demo.service.CustomerService
import com.example.demo.utils.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile

@RestController
class CustomerController {
    @Autowired
    lateinit var customerService: CustomerService
    @Autowired
    lateinit var amazonClient: AmazonClient
    @Autowired
    lateinit var customerRepository: CustomerRepository

    @PostMapping("/customer")
    fun getAllCinema(): ResponseEntity<Any> {
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapCustomerDto(customerService.getCustomers()))
    }

    @PostMapping("/user/register")
    fun uploadProfilePic(@RequestBody customerRegister: Customer): ResponseEntity<Any> {
        var customer = customerService.register(customerRegister)
        customer?.let { return ResponseEntity.ok(MapperUtil.INSTANCE.mapCustomerDto(customer)) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }
    @PostMapping("/user/history")
    fun getTicketHistory(@RequestBody customerRequest: Customer): ResponseEntity<Any> {
        var bookings = customerService.getHistory(customerRequest)
        bookings?.let { return ResponseEntity.ok(MapperUtil.INSTANCE.mapBookingNoCustomer(bookings)) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }


    @PutMapping("/user/image/{userId}")
    fun uploadProfilePic(@RequestPart(value = "file") file: MultipartFile, @PathVariable("userId") id: Long?): ResponseEntity<Any> {
        var image = this.amazonClient.uploadFile(file)
        image?.let {
            return ResponseEntity.ok(MapperUtil.INSTANCE.mapCustomerDto(customerService.setProfile(id, image)))
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }
    @PutMapping("/user/edit/{userId}")
    fun editUserInfo(@PathVariable("userId") id: Long?,@RequestBody customerInfo: Customer): ResponseEntity<Any> {

        var customer = customerService.getCustomerById(id)
        customer?.let {
            return ResponseEntity.ok(MapperUtil.INSTANCE.mapCustomerDto(customerService.setInfo(customer,customerInfo)))
        }


        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @DeleteMapping("/deleteFile")
    fun deleteFile(@RequestPart(value = "url") fileUrl: String): ResponseEntity<*> {
        return ResponseEntity.ok(this.amazonClient.deleteFileFromS3Bucket(fileUrl))
    }
}