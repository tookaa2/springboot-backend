package com.example.demo.controller

import com.example.demo.service.SeatService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class SeatController{
    @Autowired
    lateinit var seatService: SeatService
    @PostMapping("/seat")
    fun getAllMovies(): ResponseEntity<Any> {
        return ResponseEntity.ok(seatService.getSeats())
    }
}