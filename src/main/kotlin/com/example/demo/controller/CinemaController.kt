package com.example.demo.controller

import com.example.demo.service.CinemaService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class CinemaController{
    @Autowired
    lateinit var cinemaService: CinemaService
    @PostMapping("/cinemas")
    fun getAllCinema(): ResponseEntity<Any> {
        return ResponseEntity.ok(cinemaService.getCinema())
    }
}