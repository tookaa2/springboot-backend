package com.example.demo.dao

import com.example.demo.entity.SeatObject

interface SeatObjectDao{
    fun getSeat():List<SeatObject>
}