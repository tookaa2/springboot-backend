package com.example.demo.dao

import com.example.demo.entity.SeatObject
import com.example.demo.repository.SeatObjectRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class SeatObjectDaoImpl:SeatObjectDao{
    @Autowired
    lateinit var seatObjectRepository: SeatObjectRepository
    override fun getSeat(): List<SeatObject> {
        return seatObjectRepository.findAll().filterIsInstance(SeatObject::class.java)
    }

}