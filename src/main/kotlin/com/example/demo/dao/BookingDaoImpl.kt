package com.example.demo.dao

import com.example.demo.entity.dto.BookingRequest
import com.example.demo.entity.Booking
import com.example.demo.entity.SeatObject
import com.example.demo.entity.SeatStatus
import com.example.demo.repository.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import java.security.Timestamp
import java.time.LocalDate
import javax.transaction.Transactional
import kotlin.streams.asSequence

@Profile("db")
@Repository
class BookingDaoImpl : BookingDao {
    @Transactional
    override fun save(id: Long, bookingRequest: BookingRequest): Booking {
        var customer1 = customerRepository.findById(id).orElse(null)
        var seatList = mutableListOf<SeatObject>()
        var showtime=showtimeRepository.findById(bookingRequest.showtimeId!!).orElse(null)
        for (seat in bookingRequest.seats) {
            var tempSeat = seatObjectRepository.findById(seat.id!!).orElse(null)

            if( tempSeat.status === SeatStatus.BOOKED)
            return error("Seat not available")
        }
        for (seat in bookingRequest.seats) {
            var tempSeat = seatObjectRepository.findById(seat.id!!).orElse(null)
            tempSeat.status = SeatStatus.BOOKED
            seatList.add(tempSeat)
        }
        val source = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        val qrCode= java.util.Random().ints(18, 0, source.length)
                .asSequence()
                .map(source::get)
                .joinToString("")
        var booking = bookingRepository.save(Booking(showtime,  System.currentTimeMillis(),customer1,qrCode))
        booking.seats=seatList

        customer1.bookingList.add(booking)
        return booking
    }
    @Autowired
    lateinit var showtimeRepository: ShowtimeRepository

    @Autowired
    lateinit var bookingRepository: BookingRepository
    @Autowired
    lateinit var customerRepository: CustomerRepository
    @Autowired
    lateinit var seatObjectRepository: SeatObjectRepository

    override fun getAllBookings(): List<Booking> {
        return bookingRepository.findAll().filterIsInstance(Booking::class.java)
    }

}