package com.example.demo.dao

import com.example.demo.entity.Showtime
import com.example.demo.entity.dto.ShowtimeAddDto
import com.example.demo.entity.dto.ShowtimeRequest

interface ShowtimeDao{
    fun getAllShowtime():List<Showtime>
    fun getShowtimeByMovie(id: Long, showtimeRequest: ShowtimeRequest): List<Showtime>?
    fun saveShowtime(showtimeAddDto: ShowtimeAddDto): Showtime
}