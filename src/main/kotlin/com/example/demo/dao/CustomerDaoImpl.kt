package com.example.demo.dao

import com.example.demo.entity.Customer
import com.example.demo.entity.UserStatus
import com.example.demo.repository.CustomerRepository
import com.example.demo.security.entity.AuthorityName
import com.example.demo.security.entity.JwtUser
import com.example.demo.security.repository.AuthorityRepository
import com.example.demo.security.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Repository
import javax.transaction.Transactional

@Profile("db")
@Repository
class CustomerDaoImpl : CustomerDao {
    override fun setInfo(customer: Customer, customerInfo: Customer): Customer {
        var cusomerToSave =customer
        cusomerToSave.firstname=customerInfo.firstname
        cusomerToSave.lastname=customerInfo.lastname
        return customerRepository.save(cusomerToSave)
    }

    @Autowired
    lateinit var authorityRepository: AuthorityRepository
    @Autowired
    lateinit var userRepository: UserRepository

    @Transactional
    override fun register(customer: Customer): Customer? {
        val encoder = BCryptPasswordEncoder()
        var customerAuth = authorityRepository.findByName(AuthorityName.ROLE_CUSTOMER)
        var customerToRegist = Customer(
                firstname = customer.firstname,
                lastname = customer.lastname,
                email = customer.email,
                password = encoder.encode(customer.password),
                userStatus = UserStatus.ACTIVE
        )
        var jwtCustomer = JwtUser(
                username = customer.email,
                password = encoder.encode(customer.password),
                email = customer.email,
                enabled = true,
                firstname = customer.firstname,
                lastname = customer.lastname
        )
        customerRepository.save(customerToRegist)
        userRepository.save(jwtCustomer)
        customerToRegist.jwtUser = jwtCustomer
        jwtCustomer.user = customerToRegist
        jwtCustomer.authorities.add(customerAuth)
        return customerRepository.save(customerToRegist)
    }

    override fun setProfile(id: Long?, image: String): Customer? {
        var customer = customerRepository.findById(id!!).orElse(null)
        customer?.let {
            customer.imageUrl = image
            return customerRepository.save(customer)

        }
        return null
    }

    override fun getCustomerById(id: Long?): Customer {
        return customerRepository.findById(id!!).orElse(null)
    }

    override fun getCustomers(): List<Customer> {
        return customerRepository.findAll().filterIsInstance(Customer::class.java)
    }

    @Autowired
    lateinit var customerRepository: CustomerRepository
}