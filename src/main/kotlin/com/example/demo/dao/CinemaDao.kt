package com.example.demo.dao

import com.example.demo.entity.Cinema

interface CinemaDao{
    fun getCinema():List<Cinema>
}