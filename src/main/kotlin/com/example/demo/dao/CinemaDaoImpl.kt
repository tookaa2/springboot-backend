package com.example.demo.dao

import com.example.demo.entity.Cinema
import com.example.demo.repository.CinemaRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class CinemaDaoImpl:CinemaDao{
    @Autowired
    lateinit var cinemaRepository: CinemaRepository
    override fun getCinema(): List<Cinema> {
        return cinemaRepository.findAll().filterIsInstance(Cinema::class.java)
    }

}