package com.example.demo.dao

import com.example.demo.entity.Seat

interface SeatDao{
    fun getSeats():List<Seat>
}