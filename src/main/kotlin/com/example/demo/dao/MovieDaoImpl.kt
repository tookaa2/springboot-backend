package com.example.demo.dao

import com.example.demo.entity.Movie
import com.example.demo.repository.MovieRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class MovieDaoImpl:MovieDao{
    override fun getMoviesWithPage(page: Int, pageSize: Int): Page<Movie> {
        return movieRepository.findAll(PageRequest.of(page,pageSize))
    }

    override fun deleteMovie(id: Long?): Movie {
        var movie = movieRepository.findById(id!!).orElse(null)
        movie?.let {
            movie.isDeleted=true
            return movieRepository.save(movie)
        }
        return error("no movie found with that id")

    }

    @Autowired
    lateinit var movieRepository: MovieRepository
    override fun getMovies(): List<Movie> {
        return  movieRepository.findByIsDeletedIsFalse()!!.filterIsInstance(Movie::class.java)
    }

}