package com.example.demo.dao

import com.example.demo.entity.dto.BookingRequest
import com.example.demo.entity.Booking

interface BookingDao{
    fun getAllBookings():List<Booking>
    fun save(id: Long, bookingRequest: BookingRequest):Booking
}