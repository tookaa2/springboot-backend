package com.example.demo.dao

import com.example.demo.entity.Showtime
import com.example.demo.entity.dto.ShowtimeAddDto
import com.example.demo.entity.dto.ShowtimeRequest
import com.example.demo.repository.CinemaRepository
import com.example.demo.repository.MovieRepository
import com.example.demo.repository.ShowtimeRepository
import com.example.demo.utils.SeatPlanFactoryInterface
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import java.sql.Timestamp
import javax.transaction.Transactional

@Profile("db")
@Repository
class ShowtimeDaoImpl : ShowtimeDao {
    @Autowired
    lateinit var movieRepository:MovieRepository
    @Autowired
    lateinit var cinemaRepository: CinemaRepository
    @Autowired
    lateinit var seatPlanFactoryInterface: SeatPlanFactoryInterface
    @Transactional
    override fun saveShowtime(showtimeAddDto: ShowtimeAddDto): Showtime {
        var movie = movieRepository.findByName(showtimeAddDto.movieTitle!!)
        movie?.let {

            var showtime = showtimeRepository.save(Showtime(
                    Timestamp(showtimeAddDto.startTime!!.toLong()).time,
                    Timestamp(showtimeAddDto.startTime!!.toLong()+movie.duration*60000).time,
                    movie,
                    cinemaRepository.findByName(showtimeAddDto.cinemaName!!)!!

            ))

            showtime.seats.addAll(seatPlanFactoryInterface.parseCinemaSeat(
                    cinemaRepository.findByName(showtimeAddDto.cinemaName!!)!!.seat

            ))
            showtime.soundtrack = showtimeAddDto.soundtrack
            showtime.subtitle = showtimeAddDto.subtitle
            return showtime
        }





        return error("No movie found from given title")









    }

    override fun getShowtimeByMovie(id: Long, showtimeRequest: ShowtimeRequest): List<Showtime>? {
        var showtimes = showtimeRepository.findByMovieId(id)
        var output = mutableListOf<Showtime>()
        showtimes?.let {
            for (showtime in showtimes) {
                if ((Timestamp(showtime.startDateTime).year === Timestamp(showtimeRequest.date!!).year)
                        && (Timestamp(showtime.startDateTime).date === Timestamp(showtimeRequest.date!!).date)
                        && (Timestamp(showtime.startDateTime).month === Timestamp(showtimeRequest.date!!).month)
                ) {
                    output.add(showtime)
                }
            }
        }
        return output
    }

    @Autowired
    lateinit var showtimeRepository: ShowtimeRepository

    override fun getAllShowtime(): List<Showtime> {
        return showtimeRepository.findAll().filterIsInstance(Showtime::class.java)
    }

}