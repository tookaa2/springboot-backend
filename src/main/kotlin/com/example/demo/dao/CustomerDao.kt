package com.example.demo.dao

import com.example.demo.entity.Booking
import com.example.demo.entity.Customer

interface CustomerDao{
    fun getCustomers():List<Customer>
    fun getCustomerById(id: Long?): Customer
    fun setProfile(id: Long?, image: String): Customer?
    fun register(customer: Customer): Customer?
    fun setInfo(customer: Customer, customerInfo: Customer): Customer
}