package com.example.demo.dao

import com.example.demo.entity.Movie
import org.springframework.data.domain.Page

interface MovieDao{
    fun getMovies():List<Movie>
    fun deleteMovie(id: Long?): Movie
    fun getMoviesWithPage(page: Int, pageSize: Int): Page<Movie>
}