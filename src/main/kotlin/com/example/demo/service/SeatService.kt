package com.example.demo.service

import com.example.demo.entity.Seat

interface SeatService{
    fun getSeats():List<Seat>
}