package com.example.demo.service

import com.example.demo.entity.Movie
import org.springframework.data.domain.Page

interface MovieService{
    fun getMovies():List<Movie>
    fun deleteMovie(id: Long?): Movie
    fun getMoviesWithPage(page: Int, pageSize: Int): Page<Movie>
}