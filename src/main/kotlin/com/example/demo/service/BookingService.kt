package com.example.demo.service

import com.example.demo.entity.dto.BookingRequest
import com.example.demo.entity.Booking

interface BookingService{
    fun getAllBooking():List<Booking>
    fun save(id: Long, bookingRequest: BookingRequest): Booking
}