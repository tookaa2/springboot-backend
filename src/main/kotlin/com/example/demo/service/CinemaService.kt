package com.example.demo.service

import com.example.demo.entity.Cinema

interface CinemaService{
    fun getCinema():List<Cinema>
}