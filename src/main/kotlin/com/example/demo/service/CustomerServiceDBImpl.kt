package com.example.demo.service

import com.example.demo.dao.CustomerDao
import com.example.demo.entity.Booking
import com.example.demo.entity.Customer
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CustomerServiceDBImpl : CustomerService {
    override fun setInfo(customer: Customer, customerInfo: Customer): Customer {
        return customerDao.setInfo(customer, customerInfo)
    }

    override fun getHistory(customerRequest: Customer): List<Booking> {
        var bookingList=customerDao.getCustomerById(customerRequest.id).bookingList
        return bookingList
    }

    override fun register(customer: Customer): Customer? {
        return customerDao.register(customer)
    }

    override fun setProfile(id: Long?, image: String): Customer? {
        return customerDao.setProfile(id, image)
    }

    override fun getCustomerById(id: Long?): Customer? {
        return customerDao.getCustomerById(id)
    }

    @Autowired
    lateinit var customerDao: CustomerDao

    override fun getCustomers(): List<Customer> {
        return customerDao.getCustomers()
    }

}