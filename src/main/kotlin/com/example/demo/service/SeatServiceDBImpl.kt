package com.example.demo.service

import com.example.demo.dao.SeatDao
import com.example.demo.entity.Seat
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class SeatServiceDBImpl:SeatService{
    @Autowired
    lateinit var seatDao: SeatDao
    override fun getSeats(): List<Seat> {
       return seatDao.getSeats()
    }

}