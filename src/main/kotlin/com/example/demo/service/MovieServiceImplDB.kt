package com.example.demo.service

import com.example.demo.dao.MovieDao
import com.example.demo.entity.Movie
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class MovieServiceImplDB:MovieService{
    override fun getMoviesWithPage(page: Int, pageSize: Int): Page<Movie> {
        return movieDao.getMoviesWithPage(page,pageSize)
    }

    override fun deleteMovie(id: Long?): Movie {
        return movieDao.deleteMovie(id)
    }

    @Autowired
    lateinit var movieDao: MovieDao
    override fun getMovies(): List<Movie> {
        return  movieDao.getMovies()
    }

}