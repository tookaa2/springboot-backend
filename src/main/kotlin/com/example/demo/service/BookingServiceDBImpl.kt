package com.example.demo.service

import com.example.demo.dao.BookingDao
import com.example.demo.entity.dto.BookingRequest
import com.example.demo.entity.Booking
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class BookingServiceDBImpl:BookingService{
    override fun save(id: Long, bookingRequest: BookingRequest): Booking {
        return  bookingDao.save(id,bookingRequest)
    }

    @Autowired
    lateinit var bookingDao: BookingDao
    override fun getAllBooking(): List<Booking> {
        return bookingDao.getAllBookings()
    }

}