package com.example.demo.service

import com.example.demo.entity.Booking
import com.example.demo.entity.Customer

interface CustomerService{
    fun getCustomers():List<Customer>
    fun getCustomerById(id: Long?): Customer?
    fun setProfile(id: Long?, image: String): Customer?
    fun register(customerRegister: Customer): Customer?
    fun getHistory(customerRequest: Customer): List<Booking>
    fun setInfo(customer: Customer, customerInfo: Customer): Customer

}