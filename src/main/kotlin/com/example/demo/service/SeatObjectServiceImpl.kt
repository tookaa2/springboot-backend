package com.example.demo.service

import com.example.demo.dao.SeatObjectDao
import com.example.demo.entity.SeatObject
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class SeatObjectServiceImpl:SeatObjectService{
    @Autowired
    lateinit var seatObjectDao: SeatObjectDao
    override fun getSeats(): List<SeatObject> {
        return seatObjectDao.getSeat()
    }

}