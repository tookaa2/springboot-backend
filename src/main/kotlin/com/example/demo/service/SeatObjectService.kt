package com.example.demo.service

import com.example.demo.entity.SeatObject

interface SeatObjectService{
    fun getSeats():List<SeatObject>
}