package com.example.demo.service

import com.example.demo.dao.CinemaDao
import com.example.demo.entity.Cinema
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CinemaServiceImpl:CinemaService{
    override fun getCinema(): List<Cinema> {
        return cinemaDao.getCinema()
    }

    @Autowired
    lateinit var cinemaDao: CinemaDao

}