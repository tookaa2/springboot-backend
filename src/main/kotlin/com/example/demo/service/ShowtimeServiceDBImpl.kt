package com.example.demo.service

import com.example.demo.dao.ShowtimeDao
import com.example.demo.entity.Showtime
import com.example.demo.entity.dto.ShowtimeAddDto
import com.example.demo.entity.dto.ShowtimeRequest
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ShowtimeServiceDBImpl:ShowtimeService{
    override fun saveShowtime(showtimeAddDto: ShowtimeAddDto): Showtime {
        return showtimeDao.saveShowtime(showtimeAddDto)
    }

    override fun getShowtimeByMovie(id: Long, showtimeRequest: ShowtimeRequest): List<Showtime>? {
        return showtimeDao.getShowtimeByMovie(id,showtimeRequest)
    }

    @Autowired
    lateinit var showtimeDao: ShowtimeDao
    override fun getAllShowtime(): List<Showtime> {
        return showtimeDao.getAllShowtime()
    }

}