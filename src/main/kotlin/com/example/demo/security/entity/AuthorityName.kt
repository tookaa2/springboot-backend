package com.example.demo.security.entity

enum class AuthorityName{
    ROLE_CUSTOMER, ROLE_ADMIN, ROLE_GENERAL
}